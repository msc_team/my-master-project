# Initialize the emulator and layers
emu = Emulator()
base = Base()
routing = Routing()
ebgp = Ebgp()
web = WebService()

###############################################################################
# Create an Internet Exchange
# Modify the Internet Exchange ID if needed
base.createInternetExchange(200)

###############################################################################
# Create and set up your Autonomous System
# Modify the AS number, network names, router names, etc.
my_as = base.createAutonomousSystem(200)

# Create multiple networks and routers
for i in range(3):
    network_name = f'my_network{i}'
    router_name = f'my_router{i}'
    my_as.createNetwork(network_name)
    my_as.createRouter(router_name).joinNetwork(network_name).joinNetwork(f'ix200{i}')
    my_as.createHost(f'my_web{i}').joinNetwork(network_name)

# Install and bind your web service
for i in range(3):
    web_service_name = f'my_web_service{i}'
    node_name = f'my_web{i}'
    my_as.createHost(node_name).joinNetwork(f'my_network{i}')  # Join the host to the network if not already done
    web.install(web_service_name)
    emu.addBinding(Binding(web_service_name, filter=Filter(nodeName=node_name, asn=200)))  # Modify AS number and node name

###############################################################################
# Peering configuration
# Modify the peering relationships as needed
for i in range(3):
    peer_asn = 201 + i
    ebgp.addRsPeer(200, peer_asn)

###############################################################################
# Rendering and Compilation
# These lines remain the same unless you want to change the output directory
emu.addLayer(base)
emu.addLayer(routing)
emu.addLayer(ebgp)
emu.addLayer(web)

emu.render()
emu.compile(Docker(), './my_output')  # Modify the output directory if needed
